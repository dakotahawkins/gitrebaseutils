# Utilities for git rebase #

These utilities support my particular rebase workflow (explained somewhat below). They are intended to remember file timestamps pre-rebase and re-apply them post-rebase, but only for files that wouldn't be modified as a result of the rebase.

At work, I develop on a personal fork of our main repository, which allows me to rewrite my history as much as I want before I submit a pull request. I enjoy rewriting my history because I like to re-organize my commits and clean things up before I submit my changes for code review(s) and create pull requests.

In this scenario, I'm normally working on my clone of my fork with a feature branch off of one of our main branches. When I want to merge changes in from the upstream main branch, I normally:

* Update the main branch "in the background" by doing a fetch and then pushing from the origin main branch to my local copy
* Rebase my feature branch onto the main branch

The problem I've run into with this is that since the rebase rolls back my commits and re-applies them, I wind up with updated file timestamps on the files I've changed, regardless of whether the files wind up modified by the rebase. This can, with a large code base, waste a lot of unnecessary time building because our build system (like most) simply compares timestamps to determine whether build output is out of date.

### How do I get set up? ###

Clone the repository, and either use `git-rebase-utils.sh` directly, or run `git-rebase-utils.sh install` to create global git aliases for each command in the script.

### Documentation ###

```text
Usage:
    Without installing:
        git-rebase-utils.sh [command] [arguments...]

    After installing:
        git [command] [arguments...]

    Commands:
        help-rebase-utils
        rebase-utils-help
            Displays this usage information.

        install
            Installs these commands as global git aliases.

        update [branch ...]
            Runs a git fetch and then pushes fetched updates onto the branches
            specified, effectively updating them in the background.

            Branches specified must be valid local branches and must not be your
            current branch.

        list-changes-for [branch]
            Lists changed files on your current branch with respect to the
            branch specified.

            Branch specified must be a valid local branch and must not be your
            current branch.

        list-updates-from [branch]
            Lists updated files on the branch specified with respect to your
            current branch.

            Branch specified must be a valid local branch and must not be your
            current branch.

        rebase-ts [branch]
            Runs a git rebase onto the supplied branch, but attempts to preserve
            the file modification timestamps of files that are not modified by
            the rebase operation.

            In the event of a merge conflict, resolve the conflicts normally and
            then either continue the rebase using the rebase-ts-continue command
            or abort the rebase using the rebase-ts-abort command. This makes
            sure the timestamps are re-applied when the rebase is eventually
            successful.

            Branch specified must be a valid local branch and must not be your
            current branch.

        rebase-ts-continue
            Runs a git rebase --continue and attempts to re-apply timestamps
            saved by rebase-ts.

        rebase-ts-abort
            Runs a git rebase --abort and attempts to re-apply timestamps saved
            by rebase-ts.

        save-ts [branch]
            Saves file modification timestamps and checksums for local files
            that differ from the supplied branch.

            Branch specified must be a valid local branch and must not be your
            current branch.

        apply-ts
            Re-applies file modification timestamps saved by save-ts if their
            current checksum matches the saved checksum.
```
