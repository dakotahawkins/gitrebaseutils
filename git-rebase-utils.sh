#!/bin/bash

########################################################################################################################
# Use without installing:
#   git-rebase-utils.sh [command] [arguments...]
#
# Use after installing:
#   git [command] [arguments...]
#
# Commands:
#   help-rebase-utils
#   rebase-utils-help
#       Displays usage information.
#
#   install
#       Installs these commands as global git aliases.
#
#   update [branch ...]
#       Runs a git fetch and then pushes fetched updates onto the branches specified, effectively updating them in the
#       background.
#
#       Branches specified must be valid local branches and must not be your current branch.
#
#   list-changes-for [branch]
#       Lists changed files on your current branch with respect to the branch specified.
#
#       Branch specified must be a valid local branch and must not be your current branch.
#
#   list-updates-from [branch]
#       Lists updated files on the branch specified with respect to your current branch.
#
#       Branch specified must be a valid local branch and must not be your current branch.
#
#   rebase-ts [branch]
#       Runs a git rebase onto the supplied branch, but attempts to preserve the file modification timestamps of files
#       that are not modified by the rebase operation.
#
#       In the event of a merge conflict, resolve the conflicts normally and then either continue the rebase using the
#       rebase-ts-continue command or abort the rebase using the rebase-ts-abort command. This makes sure the timestamps
#       are re-applied when the rebase is eventually successful.
#
#       Branch specified must be a valid local branch and must not be your current branch.
#
#   rebase-ts-continue
#       Runs a git rebase --continue and attempts to re-apply timestamps saved by rebase-ts.
#
#   rebase-ts-abort
#       Runs a git rebase --abort and attempts to re-apply timestamps saved by rebase-ts.
#
#   save-ts [branch]
#       Saves file modification timestamps and checksums for local files that differ from the supplied branch.
#
#       Branch specified must be a valid local branch and must not be your current branch.
#
#   apply-ts
#       Re-applies file modification timestamps saved by save-ts if their current checksum matches the saved checksum.
########################################################################################################################

main() {
    git_dir=$(git rev-parse --git-dir)
    if ! [[ $? -eq 0 && -d "$git_dir" ]]; then
        error-exit "Must be executed from the root of a git repository."
    fi

    if [[ "$#" -lt "1" ]]; then
        error-exit "Must supply a command and its arguments."
    fi

    local command="$1"
    declare -a command_args=("${@:2}")

    if [[ "$command" == "update" ]]; then
        if [[ "${#command_args[@]}" -lt "1" ]]; then
            error-exit "Must supply branches to update."
        fi

        validate-branch-names "${command_args[@]}"
        update "${command_args[@]}"
    elif [[ "$command" == "list-changes-for" ]]; then
        if [[ "${#command_args[@]}" -ne "1" ]]; then
            error-exit "Must supply a single branch."
        fi

        validate-branch-names "${command_args[@]}"
        list-changes-for "${command_args[@]}"
    elif [[ "$command" == "list-updates-from" ]]; then
        if [[ "${#command_args[@]}" -ne "1" ]]; then
            error-exit "Must supply a single branch."
        fi

        validate-branch-names "${command_args[@]}"
        list-updates-from "${command_args[@]}"
    elif [[ "$command" == "save-ts" ]]; then
        if [[ "${#command_args[@]}" -ne "1" ]]; then
            error-exit "Must supply a single branch for comparison."
        fi

        validate-branch-names "${command_args[@]}"
        save-ts "${command_args[@]}"
    elif [[ "$command" == "apply-ts" ]]; then
        if [[ "${#command_args[@]}" -ne "0" ]]; then
            error-exit "Call apply-ts without additional arguments."
        fi

        apply-ts
    elif [[ "$command" == "rebase-ts" ]]; then
        if [[ "${#command_args[@]}" -ne "1" ]]; then
            error-exit "Must supply a single branch."
        fi

        validate-branch-names "${command_args[@]}"
        rebase-ts "${command_args[@]}"
    elif [[ "$command" == "rebase-ts-continue" ]]; then
        if [[ "${#command_args[@]}" -ne "0" ]]; then
            error-exit "Call rebase-ts-continue without additional arguments."
        fi

        rebase-ts-continue
    elif [[ "$command" == "rebase-ts-abort" ]]; then
        if [[ "${#command_args[@]}" -ne "0" ]]; then
            error-exit "Call rebase-ts-abort without additional arguments."
        fi

        rebase-ts-abort
    elif [[ "$command" == "install" ]]; then
        if [[ "${#command_args[@]}" -ne "0" ]]; then
            error-exit "Call install without additional arguments."
        fi

        install
    elif [[ "$command" == "help-rebase-utils" ]]; then
        help-rebase-utils
    elif [[ "$command" == "rebase-utils-help" ]]; then
        help-rebase-utils
    else
        error-exit "Unrecognized command."
    fi
}

update() {
    for branch; do
        echo Updating $branch...

        git fetch origin $branch:$branch
    done
}

list-changes-for() {
    git diff --name-status "$@"...HEAD | sort | uniq
}

list-updates-from() {
    git diff --name-status HEAD..."$@" | sort | uniq
}

save-ts() {
    echo Saving modified file timestamps...

    if [[ -f "$git_dir/.mtimes" ]]; then
        rm "$git_dir/.mtimes"
    fi

    for changed_file in $(git diff --name-only $1...HEAD | sort | uniq); do
        # Skip files we've deleted
        if ! [[ -f "$changed_file" ]]; then
            continue
        fi

        local timestamp=$(stat --printf="%Y" "$changed_file")
        local checksum=$(md5sum "$changed_file" | cut -d ' ' -f1)
        printf "    %s\n" "$changed_file"
        printf "        Timestamp: %s\n" "$(date -d @"$timestamp")"
        printf "        Checksum:  %s\n" "$checksum"
        printf "%s,%s,%s\n" "$changed_file" "$timestamp" "$checksum" >> "$git_dir/.mtimes"
    done

    if ! [[ -f "$git_dir/.mtimes" ]]; then
        echo "    No files found for save."
    fi
}

apply-ts() {
    echo Re-applying modified file timestamps...

    if ! [[ -f "$git_dir/.mtimes" ]]; then
        echo "    No saved timestamps found."
        return
    fi

    # Apply the timestamps to the files
    while read line; do
        # Split each line in $git_dir/.mtimes into an array (file name, timestamp, checksum)
        IFS=',' read -ra line_array <<< "$line"

        local changed_file="${line_array[0]}"
        echo "    $changed_file"

        # Bail out if the file no longer exists
        if ! [[ -f "$changed_file" ]]; then
            echo "        File not found."
            continue
        fi

        local saved_checksum="${line_array[2]}"
        local current_checksum=$(md5sum "$changed_file" | cut -d ' ' -f1)

        # The file checksum has to match our saved checksum to be eligible
        if [[ "$saved_checksum" != "$current_checksum" ]]; then
            echo "        File checksums differ, won't apply saved timestamp."
            continue
        fi

        local saved_timestamp="${line_array[1]}"
        local current_timestamp=$(stat --printf="%Y" "$changed_file")

        # Apply the saved timestamp
        touch --no-create -d @"$saved_timestamp" "$changed_file"

        printf "        Current timestamp: %s\n" "$(date -d @"$current_timestamp")"
        printf "        Replaced with:     %s\n" "$(date -d @"$saved_timestamp")"
    done <"$git_dir/.mtimes"

    rm "$git_dir/.mtimes"
}

rebase-ts() {
    save-ts "$@"

    echo Running rebase...

    # Only call apply-ts if the rebase finishes successfully.
    git rebase "$@" && apply-ts || {
        echo "Remember to continue or abort the rebase using rebase-ts-continue or rebase-ts-abort!"
    }
}

rebase-ts-continue() {
    # Only call apply-ts if the rebase finishes successfully.
    git rebase --continue && apply-ts || {
        echo "Remember to continue or abort the rebase using rebase-ts-continue or rebase-ts-abort!"
    }
}

rebase-ts-abort() {
    git rebase --abort
    apply-ts
}

validate-branch-names() {
    declare -a local_branch_names
    mapfile -t local_branch_names < <(git branch | grep -v "^*" | tr -d [:blank:])

    local has_invalid_branch=0
    for branch; do
        local is_valid=0
        for local_branch in "${local_branch_names[@]}"; do
            if [[ "$branch" == "$local_branch" ]]; then
                is_valid=1
                break
            fi
        done

        if [[ "$is_valid" -ne "1" ]]; then
            echo "Invalid branch name: $branch" >&2
            has_invalid_branch=1
        fi
    done

    if [[ "$has_invalid_branch" -eq "1" ]]; then
        error-exit "Invalid local branch names supplied as arguments."
    fi
}

install() {
    # Get the full path to this script.
    pushd $(dirname $0) > /dev/null
    local my_path=$(pwd -P)
    popd > /dev/null

    # Get the script name.
    local my_name="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"

    # Add aliases to path/name command for each command.
    git config --replace-all --global alias.help-rebase-utils "!$my_path/$my_name help-rebase-utils"
    git config --replace-all --global alias.rebase-utils-help "!$my_path/$my_name rebase-utils-help"
    git config --replace-all --global alias.update "!$my_path/$my_name update"
    git config --replace-all --global alias.list-changes-for "!$my_path/$my_name list-changes-for"
    git config --replace-all --global alias.list-updates-from "!$my_path/$my_name list-updates-from"
    git config --replace-all --global alias.rebase-ts "!$my_path/$my_name rebase-ts"
    git config --replace-all --global alias.rebase-ts-continue "!$my_path/$my_name rebase-ts-continue"
    git config --replace-all --global alias.rebase-ts-abort "!$my_path/$my_name rebase-ts-abort"
    git config --replace-all --global alias.save-ts "!$my_path/$my_name save-ts"
    git config --replace-all --global alias.apply-ts "!$my_path/$my_name apply-ts"
}

help-rebase-utils() {
    echo "Usage:"
    echo "    Without installing:"
    echo "        git-rebase-utils.sh [command] [arguments...]"
    echo
    echo "    After installing:"
    echo "        git [command] [arguments...]"
    echo
    echo "    Commands:"
    echo "        help-rebase-utils"
    echo "        rebase-utils-help"
    echo "            Displays this usage information."
    echo
    echo "        install"
    echo "            Installs these commands as global git aliases."
    echo
    echo "        update [branch ...]"
    echo "            Runs a git fetch and then pushes fetched updates onto the branches"
    echo "            specified, effectively updating them in the background."
    echo
    echo "            Branches specified must be valid local branches and must not be your"
    echo "            current branch."
    echo
    echo "        list-changes-for [branch]"
    echo "            Lists changed files on your current branch with respect to the"
    echo "            branch specified."
    echo
    echo "            Branch specified must be a valid local branch and must not be your"
    echo "            current branch."
    echo
    echo "        list-updates-from [branch]"
    echo "            Lists updated files on the branch specified with respect to your"
    echo "            current branch."
    echo
    echo "            Branch specified must be a valid local branch and must not be your"
    echo "            current branch."
    echo
    echo "        rebase-ts [branch]"
    echo "            Runs a git rebase onto the supplied branch, but attempts to preserve"
    echo "            the file modification timestamps of files that are not modified by"
    echo "            the rebase operation."
    echo
    echo "            In the event of a merge conflict, resolve the conflicts normally and"
    echo "            then either continue the rebase using the rebase-ts-continue command"
    echo "            or abort the rebase using the rebase-ts-abort command. This makes"
    echo "            sure the timestamps are re-applied when the rebase is eventually"
    echo "            successful."
    echo
    echo "            Branch specified must be a valid local branch and must not be your"
    echo "            current branch."
    echo
    echo "        rebase-ts-continue"
    echo "            Runs a git rebase --continue and attempts to re-apply timestamps"
    echo "            saved by rebase-ts."
    echo
    echo "        rebase-ts-abort"
    echo "            Runs a git rebase --abort and attempts to re-apply timestamps saved"
    echo "            by rebase-ts."
    echo
    echo "        save-ts [branch]"
    echo "            Saves file modification timestamps and checksums for local files"
    echo "            that differ from the supplied branch."
    echo
    echo "            Branch specified must be a valid local branch and must not be your"
    echo "            current branch."
    echo
    echo "        apply-ts"
    echo "            Re-applies file modification timestamps saved by save-ts if their"
    echo "            current checksum matches the saved checksum."
}

error-exit() {
    echo "$1" >&2
    echo
    help-rebase-utils
    exit 1
}

main "$@"
exit 0
